## Installing C64 OS On Vice
The instructions below assume you have a familiarity working with GTK VICE on Windows, and that you are familiar working with the settings for the emulator.

Although support for emulated CMD Devices like the CMD HD and RamLink are new (:see r1) it is quite stable and is a wonderful platform to run C64 OS on.

## Configuration VICE For Installing C64 OS

In order to install C64 OS on an emulated CMD HD with VICE there are few things we will need to setup before we get started. There are links at the bottom of this documentation to help you find the files you need. At the end of this section, we should have a functioning C64 emulated in VICE with a number of drives setup using JiffyDOS.

1. **VICE > 3.5** -- this is the first version that supports CMD HD
2. **CMD HD Boot Rom >= 2.80** -- this is the latest version and seems to work the best
3. **JiffyDOS** -- this really helps with navigation and speeds up C64 OS although it is optional
4. **CMD HD Utilitles** -- this is optional as well but recommended, it has a file copy utility that is used in the tutorial
5. **Virtual Filesystem** -- familiarity with setting up a Virtual Filesystem on VICE. This is only needed during the installation to copy files. It is not required when running C64 OS.

To get started, let's first configure VICE to use CMD HD Boot Rom and JiffyDOS.

If you want to have JiffyDOS, change the KERNAL ROM to use the binary located on your computer.

![](./images/vice_machine_rom_jiffydos.png)

Then, modify the MACHINE DRIVE ROMS to use the CMDHD Boot Rom and also change the 1541 ROM to use the appropriate JiffyDOS ROM.

Note:
If you want to use a different emulated FLOPPY DRIVE instead of 1541, please use the appropriate ROM for that DRIVE instead.

![](images/vice_machine_drive_rom_cmdhd.png)

At this point, SAVE your configuration by selecting PREFERENCES > SAVE SETTINGS from the MENU.

## Setup Disk Drives

Next, we are going to configure the Drives that you will need for installing C64 OS. 

For our configuration we are going to have Drive 8 loaded with the CMD HD Utilities disk. Drive 9 will host our CMDHD. Drive 10 will point to the location of our C64 OS files for installation. Let's get started.

### Drive 8 

First, configure you Drive 8 to use a compatible Floppy Drive, and one that you installed the JiffyDos ROM for. In the image below I am selecting a 1541-2 drive, but anything should work.

Make sure you save your VICE SETTINGS at this time.

![](images/vice_drive8_config.png)

After the drive has been configured, attach the CMD HD Utilities disk to your drive.

Select attach from the File menu, and select Drive 8.

![](images/vice_drive8_attach_menu.png)

And then attach the image containing the CMD HD Utilities. Make sure you do not select Autoload.

![](images/vice_drive8_attach.png)

Now that the disk image is attached, let's verify that the disk has what we need by using the ``@$`` command to view the directory on Drive 8. It should look something like the image below.

![](images/vice_drive8_dir.png)

If you see the correct files then we can move on to the next topic.

## Drive 9

Next we are going to setup Drive 9 to use a new CMD HD image from our computer.

Before we attach the image, we need to create it. And thankfully, the VICE team has made this easy for us. All we need to do is create a new file on our computer that has a ``dhd`` extension on it. 

The simplest method I have found is to create a new ``text document`` on your computer.

![](images/vice_drive9_create_file.png)

And then rename that file to anything you want with a ``dhd`` extension. To make things simpler for us right now, rename the file to ``C64 OS.dhd``.

![](images/vice_drive9_rename.png)

Now that the image is created, we can attach it to our emulator.

First, we need to configure Drive 9 to emulate a CMD HD and optionally specify the size of the image.

Select ``CMD-HD`` as the Drive Type for Drive 9, and enter a number for the size of the image. The default is 8G, and that has been more than enough for me. I have not tried any other sizes, so your mileage may vary.

Make sure you save your VICE SETTINGS at this time.

![](images/vice_drive9_config.png)

Now let's attach the image we just created to Drive 9. 

Select attach from the File menu, and pick Drive 9.

![](images/vice_drive9_attach_menu.png)

Next, find the image you just created and Attach it. Again, make sure you do not select Autoload here.

![](images/vice_drive9_attach.png)

Also, note that when you select the disk image that VICE will not recognize the contents of the disk. This is completely normal, and you can ignore it for now. It will be fine after we are done.
### Create System

The next step is very important, we need to create a new System Area on our new image. This needs to be done first before you can add any new partitions to your disk.

To start with, load and run the ``CREATE SYS`` program that is on your CMD HD Utilities disk.

![](images/vice_drive9_load_sys.png)

When you run the program, you will be greeted with a screen that describes what this tool will be doing. 

![](images/vice_drive9_ignore_config.png)

You will notice near the bottom, that the program is asking you to switch the drive into ``installation`` mode by pressing the SWAP button while pressing the RESET button. You can ignore this becuse the greate VICE developers automatically put the drive in this mode when a new zero byte image is attached.

After you press enter, you will be greeted by another screen that gives you the option to change the starting block. There is no reason to change this, so you can just press ENTER to continue.

![](images/vice_drive9_config_ready.png)

Next, you will be asked if you want to Clear the Area below the system. You should select Y here because we are not going to share the disk image with any other systems.

At this point, the computer will start building the system image on your disk. This is a good time to use the WARP MODE on VICE to speed up the process. You should see something similar to the image below when it is done.

![](images/vice_drive9_sys_finished.png)

After it is completed, you are asked to RESET the drive. This is easily accomplished by using the Drive menu on the VICE interface and selecting RESET DRIVE #9.

![](images/vice_drive9_reset.png)

However, in my experience, this does not work very well. Instead, the best course of action is to CLOSE VICE and then re-attach the disk images. This will ensure that space has been allocated for your image, and you won't have to start over again if VICE crashes.

Pro Tip:
Before continuing, make a copy of your image file so that you do not need to do this step again. I have a ``BLANK`` CMD HD image that I keep on hand so that I can copy it whenever I need, for other projects.
### Create C64 OS Partition

Finally, we are ready to create a partition for installing C64 OS. Sometimes, the RESET does not work here and you will be forced to restart VICE and re-attach the disk images. I am not sure if this is a bug in VICE or in the way I am doing this process.

Load and run the ``HD-TOOLS.64`` program which will let us add some partitions to this new disk image.

![](images/vice_drive9_load_utils.png)

After running the program, you will be greeted by a message asking you to put the disk into CONFIGUREATION MODE.

![](images/vice_drive9_config_mode.png)

 This time, we will need to do this. Again, the VICE team has made this easy for us by putting it on the DRIVE menu.

![](images/vice_drive9_config_mode_select.png)

After you have selected to go into CONFIGURATION MODE, you can press enter and you should now see the menu. There might be a slight delay until the drive gets into the correct mode.

![](images/vice_drive9_config_menu.png)

From the menu, select option 4 which will then load the partition editor. By default, you will be dropped into creating a new partition. The settings you want for the partition are shown in the image below.

![](images/vice_drive9_partition.png)

Make sure that you have select NATV, or Native type for the partition, and also make sure you have allocated the maximum number of blocks by pressing the ``MINUS`` key which will give you 65280 blocks total. You can name the partition anything you want.

After you press ENNTER on the name field, you will be asked it you want to ``WRITE NEW TABLE TO HD``, press Y here to permanently save your changes.

![](images/vice_drive9_partition_save.png)

You should then be taken back to the main menu. From here I like to validate that my partition was created by doing a couuple things.

First, I use menu option three to show me the current partition table, it should look similar to the image below with the new partition we just created.

![](images/vice_drive9_partition_view.png)

The other method is to drop back into basic and actually view the partition using JiffyDos. Like before, you can try to use the RESET DRIVE #9 option, but I have found that this does not work very well. Instead, simply CLOSE VICE and then re-attach the disk images again.

Note:
This is probably the last time you will need to do this. I promise.

Notice now that when you re-attach the CMD HD image, that you will see a beautiful image of your new partition that you just created.

![](images/vice_drive9_attach_partition.png)

You can even verify this more by changing the DRIVE using JiffyDos and then viewing the default directory.

![](images/vice_drive9_dir.png)

Congratulations, your CMD HD image is now ready to install C64 OS. But, we have one more setup step to complete before we can begin.

## Drive 10

In this step, we are going to configure Drive 10 to use our computers filesystem as a disk, and point it to the location where we have the installation files for C64 OS.

Let's go into VICE settings and enable using the Virtual Filesystem by enabling the ENABLE VIRTUAL DEVICES setting. I have found that this gives you a little faster disk access, especially when copying. 

![](images/vice_drive10_vfs.png)

Next, we want configure Drive 10 to use a Virtual Filesystem by setting the Drive Type to None, enabling the IEC device and then selecting HOST FILE SYSTEM from the IEC Device Type dropdown. In the end you have something similar to the following.

![](images/vice_drive10_iec.png)

Then, set the location of the C64 OS files by specifying the directory in the Filesystem Device directory for Drive 10. Also make sure that the ALLOW FILENAMES LONGER THEN 16 CHARACTERS is enabled as well. It should look similar to the image below.

![](images/vice_drive10_filesystem.png)

Lastly, let's verify that we can see the files on Drive 10 by showing the directory with JiffyDos.

![](images/vice_drive10_dir.png)

Congratulations if you have made it this far. You are almost ready to install C64 OS. Save your VICE settings.
## Copy C64 OS Files To Partition

The last step before installing C64 OS is to copy over all of the installation files to the new partition you created on your CMD HD image.

Load and run the FCOPY program from Drive 8.

![](images/vice_fcopy_load.png)

When you first run FCOPY you should see a screen similar to the one below.

![](images/vice_fcopy_screen.png)

Notice the instructions on the bottom for changing various settings for copying. The interface is not the most intuitive, but if you follow the instructions below you should be in good shape.

First, let's change the SOURCE device by pressing F1 until it shows Drive 10. You will notice that the Device Type is showing as ????. This is perfectly normal and can be ignored.

![](images/vice_fcopy_source.png)

Next, change the TARGET Device by press F5 until it shows our CMD HD on Drive 9. The Drive Type should also show that it is a CMD HD. If not, go back and check your configurations from before.

![](images/vice_fcopy_target.png)

Next, you need to make sure that the partition we created is showing in the TARGET Device. Use the F7 key to pick the C64OS partition. Doing this should automatically set the default path to be // which is also what we want. You screen should now look similar to the image below.

![](images/vice_fcopy_target_path.png)

Next we need to pick the files we want to copy from the SOURCE to TARGET system. Press the F key, which should prompt you for a pattern, press ENTER and you should now see a list of files. Press ENTER four times to select all the files (or you can also press T to toggle all files) for the C64 OS installation. Your screen should look similar to the image below.

![](images/vice_fcopy_select.png)

Now, press the C key to copy, and then press R to select the option to REPLACE files that are existing. It will ask you to insert the TARGET disk, which is already present, so you can just press RETURN to start copying the files.

![](images/vice_fcopy_copying.png)

The RESTORE.CAR file will take the longest to copy. Feel free to use WARP speed to make it go a little faster. But even in WARP mode, it still take a few minutes to copy over. Have patience.

Note: If you ever see the status for Drive 9 to show that it is on Track 99 for a very long time when it is copying, then it probably got messed up. At this point you will need to reformat the partition, and try the copy again. I am not sure if this is a bug with VICE, or the order in which I do things. It usually works bery well for me.

After the copy is complete you should see a screen like below asking if you want to make another copy. From here you can select NO and then exit the program.

![](images/vice_fcopy_finished.png)

Congratulations, you are now ready to install C64OS.

At this point you can remove Drive 10 from your configuration as you will no longer need the Virtual Filesystem for the installation. In fact, I recommend you dont have it active while trying to install C64 OS.

## Install C64 OS

Before we begin, let's make sure that we have all the files we need. We can do that by simply listing the directory for Drive 9.

![](images/vice_install_dir.png)

Load and run the C64OS program from Drive 9.

From here, you should be able to follow the instructions from the installtion guide for a normal CMD HD installation. Make sure you specify Drive 9 as your installation Drive and partition 1 as your partition, and not Drive 11 as in the instructions.

Once again here, WARP can be your friend.
## Mouse Configuration

My settings for mouse have been hit and miss, but I have had good luck setting Control Port #1 to Mouse (1351) emulation mode.

When C64 OS is starting up, you can use the key combination of ALT-M to capture your mouse to VICE.

There seems to be a bit of a bug in either VICE or C64 OS where if you move the mouse to the very bottom of the screen it will stay there. Nothing can recover from it, except for a reboot of the system.

You can also use the Joystick to control the mouse as well, although it is not nearly as nice as using the mouse. And in order to do that you will need to reconfigure C64 OS. There are instructions available for doing that.
## Running C64 OS

To run C64 OS after installation, simply Load and Run the program C64OS again.

![](images/vice_run_startup.png)

Sometimes you may be stuck in the wrong sub-directory. To go back to the root directory, simply issue the following command before starting C64 OS.

![](images/vice_install_root.png)

### Resource Links

#### Link to latest CMD HD Boot Roms
ftp://cbm8bit.com/hardware/cmd/CMD-HD/CMD_HD_BOOTROM_v280.bin

#### Link to Latest CMD JiffyDOS Roms
ftp://cbm8bit.com/8bit/commodore/server/Commodore%208bit%20FTP/browse/hardware/cmd/CMD-JiffyDos

#### Link to CMD HD Utilities Disk Image
ftp://cbm8bit.com/hardware/cmd/CMD-HD/HD%20Utilities.d64